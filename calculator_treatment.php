<?php
//обработка формы для калькулятора

if (isset($_POST["add"])){
    if (is_double($_POST["f1"]) && is_double($_POST["f2"]) || is_numeric($_POST["f1"]) && is_numeric($_POST["f2"])){
        $first = $_POST["f1"];
        $second = $_POST["f2"];
        $result1 = $first + $second;
        echo "Результат = ". "  " .$result1;
    }else{
        unset($_POST["f1"]);
        unset($_POST["f2"]);
        echo "Неверный формат числа, или числа отсутствуют. Пожалуйста, вернитесь и исправьте ошибки."
            . '<a href="calculator.php">Исправить</a>';
    }
}
if (isset($_POST["minus"])){
    if (is_double($_POST["f1"]) && is_double($_POST["f2"]) || is_numeric($_POST["f1"]) && is_numeric($_POST["f2"])){
        $first = $_POST["f1"];
        $second = $_POST["f2"];
        $result2 = $first - $second;
        echo "Результат = ". "  " .$result2;
    }else{
        unset($_POST["f1"]);
        unset($_POST["f2"]);
        echo "Неверный формат числа, или числа отсутствуют. Пожалуйста, вернитесь и исправьте ошибки."
            . '<a href="calculator.php">Исправить</a>';
    }
}
if (isset($_POST["multiplu"])){
    if (is_double($_POST["f1"]) && is_double($_POST["f2"]) || is_numeric($_POST["f1"]) && is_numeric($_POST["f2"])){
        $first = $_POST["f1"];
        $second = $_POST["f2"];
        $result3 = $first * $second;
        echo "Результат = ". "  " .$result3;
    }else{
        unset($_POST["f1"]);
        unset($_POST["f2"]);
        echo "Неверный формат числа, или числа отсутствуют. Пожалуйста, вернитесь и исправьте ошибки."
            . '<a href="calculator.php">Исправить</a>';
    }
}
if (isset($_POST["divide"])){
    if (is_double($_POST["f1"]) && is_double($_POST["f2"]) || is_numeric($_POST["f1"]) && is_numeric($_POST["f2"])){
        $first = $_POST["f1"];
        $second = $_POST["f2"];
        $result4 = $first / $second;
        echo "Результат = ". "  " .$result4;
    }else{
        unset($_POST["f1"]);
        unset($_POST["f2"]);
        echo "Неверный формат числа, или числа отсутствуют. Пожалуйста, вернитесь и исправьте ошибки."
            . '<a href="calculator.php">Исправить</a>';
    }
}